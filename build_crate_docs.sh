#!/usr/bin/env bash

cratename() {
    case $1 in
        bitflags-core) echo bitflags_core;;
        rust-sel4) echo sel4;;
        sel4-start) echo sel4_start;;
        sel4-sys) echo sel4_sys;;
        rust-bitmap) echo bitmap;;
        acpica-sys) echo acpica_sys;;
        *) echo $1;;
    esac
}

mkdir -p upstream_docs
d=$(pwd)/upstream_docs
dd=$d/i686-sel4-unknown/doc
index=$d/index.html
truncate -s 0 $index
cp meta/docs/style.css $dd
echo '<!doctype html> <html><head><meta charset="utf-8"><body>' >> $index
cat < meta/docs/template-before.html >> $index
echo "<p>Crates documented here:</p><ul>" >> $index
for repo in bitflags-core pci rust-sel4 sel4-start sel4-sys virtio rust-bitmap acpica-sys; do
    pushd $repo
    echo "<li>$(cratename $repo) <a href=\"https://doc.robigalia.org/arm/$(cratename $repo)\">arm</a> <a href=\"https://doc.robigalia.org/i686/$(cratename $repo)\">i686</a></li>" >> $index
    env CARGO_TARGET_DIR=$d cargo doc --target i686-sel4-unknown
    env CARGO_TARGET_DIR=$d cargo doc --target arm-sel4-gnueabi
    popd
done
echo "</ul>" >> $index
cat < meta/docs/template-after.html >> $index
echo "</body></html>" >> $index
